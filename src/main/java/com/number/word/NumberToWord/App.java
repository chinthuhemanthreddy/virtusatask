package com.number.word.NumberToWord;


import com.number.word.bussiness.NumbertoWordManager;

public class App {
 
public String convert(long number) {
	String num = "";
	if(number < 0) {
		throw new NumberFormatException("Not a valid positive number");
	}
	if(number > 999999999) {
		throw new NumberFormatException("Number is out of range");
	}
	try {
	
	num = new NumbertoWordManager().convert(number);
	}catch(Exception e) {
		
	}
	return num;
}
}
