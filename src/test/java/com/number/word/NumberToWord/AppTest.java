package com.number.word.NumberToWord;

import org.junit.Test;

import junit.framework.TestCase;


public class AppTest 
    extends TestCase
{
	private App app;

	 @Test
	    public void testOnes() {
		 app = new App();
	        assertEquals("one", app.convert(1));
	        assertEquals("nine", app.convert(9));
	    }
	 
	 @Test
	    public void testTens() {
		 app = new App();
	        assertEquals("ninety two", app.convert(92));
	        assertEquals("sixty four", app.convert(64));
	    }
	 @Test
	    public void testHundreds() {
		 app = new App();
	        assertEquals("two hundred seventeen", app.convert(217));
	        assertEquals("nine hundred twelve", app.convert(912L));
	    }
	 @Test
	    public void testThousands() {
		 app = new App();
	        assertEquals("three thousand three hundred thirty three", app.convert(3333));
	        assertEquals("six thousand four hundred sixty five", app.convert(6465L));
	    }
	 
	 @Test
	    public void testTenThousands() {
		 app = new App();
	        assertEquals("twenty nine thousand nine hundred ninety nine", app.convert(29999L));
	        assertEquals("ninety seven thousand five hundred eighty six", app.convert(97586L));
	    }
        
	 @Test
	    public void testMillion() {
		 app = new App();
	        assertEquals("five hundred million one", app.convert(500000001L));
	        assertEquals("one hundred forty seven million four hundred eighty three thousand six hundred forty seven", app.convert(147483647));
	        
	        
	    }
	 
	 @Test
	    public void testNegativeCases() {
		 app = new App();
	        assertEquals("nine billion nine hundred ninety nine million nine hundred ninety nine thousand nine hundred ninety one", app.convert(9999999991L));
	        assertEquals("one hundred", app.convert(-100));
	        
	        
	    }
}
